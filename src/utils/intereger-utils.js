export function integerValue(value, defaultVal = 0) {
  value = parseInt(value);
  return !isNaN(value) && value !== false ? value : defaultVal;
}