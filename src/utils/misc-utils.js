import queryString from 'query-string';

export function queryStringWithReplacedParameter(query, parameter, value) {
  let queryParsed = queryString.parse(query);
  queryParsed[parameter] = value;
  return queryParsed;
}

export function pluralize(number, titles) {
  const cases = [2, 0, 1, 1, 1, 2];
  return `${number} ${titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[Math.min(number % 10, 5)]]}`;
}

export function pluralizeTitle(number, titles) {
  const cases = [2, 0, 1, 1, 1, 2];
  return `${titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[Math.min(number % 10, 5)]]}`;
}

export function nullString(value) {
  return value ? value : '';
}
