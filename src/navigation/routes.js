import Main from '../Main'

export const routes = [
  {
    path: '/app',
    component: Main,
    children: [
      {
        name: 'products',
        path: 'products',
        component: () => import(/* webpackChunkName: "products" */ '../modules/products/ProductsPage'),
      },
      {
        name: 'products-add',
        path: 'products/add',
        component: () => import(/* webpackChunkName: "products" */ '../modules/products/ProductEditPage'),
      },
      {
        name: 'products-edit',
        path: 'products/edit/:product_id',
        component: () => import(/* webpackChunkName: "products" */ '../modules/products/ProductEditPage'),
      },
    ]
  }
];