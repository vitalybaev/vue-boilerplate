import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state = {
  app: {
    counter: 1,
  }
};

const mutations = {
  INCREMENT_COUNTER(state) {
    state.app.counter += 1
  },
  DECREMENT_COUNTER(state) {
    state.app.counter -= 1
  },
};

export default new Vuex.Store({
  state,
  mutations,
})
