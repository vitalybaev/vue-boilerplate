export const appIncrementTimer = makeAction('INCREMENT_COUNTER');
export const appDecrementTimer = makeAction('DECREMENT_COUNTER');

function makeAction(type) {
    return ({dispatch}, ...args) => dispatch(type, ...args)
}
