import Vue from 'vue'
import {integerValue} from "../../utils/intereger-utils";
import queryString from 'query-string';

const API_BASE_URL = 'https://new.mypet-online.ru/admin-api/v3';

Vue.http.interceptors.push((request, next) => {
  request.credentials = true;
  next();
});

export default {
  getProducts(params = {}) {
    let apiParams = {};
    apiParams.offset = params.offset ? integerValue(params.offset) : 0;
    apiParams.query = params.query ? params.query : '';
    return Vue.http.get(`${API_BASE_URL}/products?${queryString.stringify(apiParams)}`).then((response) => {
      return response.body;
    });
  },
  getAddProductInfo(productId) {
    let apiParams = {};
    apiParams.product_id = productId && typeof productId !== 'undefined' ? productId : '';
    return Vue.http.get(`${API_BASE_URL}/products/new/load?${queryString.stringify(apiParams)}`).then((response) => {
      return response.body;
    });
  },
  getCategoryFilters(categoriesIds) {
    let apiParams = {};
    apiParams.categories_ids = categoriesIds && typeof categoriesIds !== 'undefined' ? categoriesIds : '';
    return Vue.http.get(`${API_BASE_URL}/products/new/load_filters?${queryString.stringify(apiParams)}`).then((response) => {
      return response.body;
    });
  },
  getColors(params = {}) {
    let apiParams = {};
    apiParams.query = params.query ? params.query : '';
    return Vue.http.get(`${API_BASE_URL}/autocomplete/product-color?${queryString.stringify(apiParams)}`).then((response) => {
      return response.body;
    });
  },
}