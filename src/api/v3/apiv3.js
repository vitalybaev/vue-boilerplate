import productsApi from './api-products';

export const apiv3 = {
  products: productsApi,
};