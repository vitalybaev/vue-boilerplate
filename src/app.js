import Vue from 'vue'
import VueMeta from 'vue-meta'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import store from './state/state'
import {routes} from "./navigation/routes"
import 'babel-polyfill'
import ElementUI from 'element-ui'
import './base/element-overrides.scss'

// Enable vue-devtools
Vue.config.devtools = true;

// Подключам Vue Meta
Vue.use(VueMeta);

// Подключаем Element
Vue.use(ElementUI);

// Подключаем роутер
Vue.use(VueRouter);

// Подключаем http слой
Vue.use(VueResource);

// Создаем роутер
let router = new VueRouter({mode: 'history', routes});

const app = new Vue({router, store}).$mount('#app');