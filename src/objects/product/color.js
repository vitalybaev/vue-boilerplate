export class Color {
  constructor(colorJSON) {
    this.Id = colorJSON.oid;
    this.Name = colorJSON.name;
    this.Type = colorJSON.type;
    this.Pattern = colorJSON.pattern;
    this.Color = colorJSON.color;
  }

  get cssBackground() {
    if (this.Type === 'pattern') {
      return `background: url("${this.Pattern}") 0% 0% / 100% no-repeat;`;
    } else {
      return `background-color: ${this.Color}`;
    }
  }
}