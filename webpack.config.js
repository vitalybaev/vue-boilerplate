const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const path = require('path');
const NODE_ENV = process.env.NODE_ENV || 'development';
const gitRevisionPlugin = new GitRevisionPlugin();

module.exports = {
  entry: {
    app: './src/app.js'
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: NODE_ENV === 'development' ? '[name].js' : '[name].[id].[chunkhash].js',
    chunkFilename: "[name].[id].[chunkhash].bundle.js",
    publicPath: NODE_ENV === 'development' ? '/' : '/dist/',
    library: '[name]',
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        exclude: [/node_modules/],
        options: {
          loaders: {
            'css': 'vue-style-loader!css-loader'
          }
        }
      },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!sass-loader'
      },
      /*{
          test: /\.css$/,
          loader: ExtractTextPlugin.extract({
              fallback: 'style-loader', use: cssLoaders
          })
      },*/
      {
        test: /\.ts/,
        loader: 'ts-loader'
      },
      {
        test: /\.html/,
        loader: 'html-loader'
      },
      {
        test: /\.(png|gif|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(NODE_ENV),
      }
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new HtmlWebpackPlugin({
      chunks: ['app'],
      template: './src/index.html'
    })
  ],
  resolve: {
    alias: {vue: 'vue/dist/vue.js'},
    extensions: ['*', '.webpack.js', '.web.js', '.ts', '.js', '.vue']
  },

  watch: NODE_ENV === 'development',

  devtool: NODE_ENV === 'development' ? 'source-map' : false
};

if (NODE_ENV === 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      }
    })
  );
}